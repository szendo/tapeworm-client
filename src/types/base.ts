export interface Tile {
    coord: Coord;
    card: TileCard;
    rotation: Rotation;
    cutColor: Color | null;
    parents: Array<Coord | null>;
}

export type Card = TileCard | CutCard;

export interface TileCard {
    type: 'TILE';
    imageId: string;
    action: Action | null;
    edges: Array<Color | null>;
}

export interface CutCard {
    type: 'CUT';
    imageId: string;
    color: Color | null;
}

export type Action = DigAction | HatchAction | PeekAction | SwapAction;

export interface DigAction {
    type: 'DIG';
    strength: Strength;
}

export interface HatchAction {
    type: 'HATCH';
    strength: Strength;
}

export interface PeekAction {
    type: 'PEEK';
}

export interface SwapAction {
    type: 'SWAP';
    strength: Strength;
}

export type Color = 'BLACK' | 'PINK' | 'RED' | 'WHITE';

export type Coord = [number, number];

export type Rotation = 0 | 1 | 2 | 3;

export type Strength = 1 | 2;

export type Direction = 0 | 1 | 2 | 3;
