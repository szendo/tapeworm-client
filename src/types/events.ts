import type {Action, Color, Coord, Rotation} from './base';

export type GameEvent = ReshuffleDeck
    | StartTurn
    | DrawCards
    | PlayTileCard
    | PlayCutCard
    | SelectColor
    | TriggerAction
    | DiscardCards
    | ForceDraw
    | UndrawCard
    | SelectSwapTarget
    | SwapCards
    | CompleteRingworm
    | EndTurn
    | Win;

interface PlayerEvent {
    playerIndex: number;
}

interface ReshuffleDeck {
    type: 'RESHUFFLE_DECK';
}

interface StartTurn extends PlayerEvent {
    type: 'START_TURN';
}

interface DrawCards extends PlayerEvent {
    type: 'DRAW_CARDS';
    cards: string[];
}

interface PlayTileCard extends PlayerEvent {
    type: 'PLAY_TILE_CARD';
    card: string;
    coord: Coord;
    rotation: Rotation;
}

interface PlayCutCard extends PlayerEvent {
    type: 'PLAY_CUT_CARD';
    card: string;
    coord: Coord;
    cutCards: string[];
}

interface SelectColor extends PlayerEvent {
    type: 'SELECT_COLOR',
    color: Color
}

interface TriggerAction extends PlayerEvent {
    type: 'TRIGGER_ACTION',
    action: Action
}

interface DiscardCards extends PlayerEvent {
    type: 'DISCARD_CARDS';
    cards: string[];
}

interface ForceDraw extends PlayerEvent {
    type: 'FORCE_DRAW';
    targetPlayerIndex: number;
    count: number;
}

interface UndrawCard extends PlayerEvent {
    type: 'UNDRAW_CARD';
    card: string;
}

interface SelectSwapTarget extends PlayerEvent {
    type: 'SELECT_SWAP_TARGET';
    targetPlayerIndex: number;
    count: number;
}

interface SwapCards extends PlayerEvent {
    type: 'SWAP_CARDS';
    targetPlayerIndex: number;
    cardsGiven: string[];
    cardsTaken: string[];
}

interface CompleteRingworm extends PlayerEvent {
    type: 'COMPLETE_RINGWORM';
    ringwormCount: number;
}

interface EndTurn extends PlayerEvent {
    type: 'END_TURN';
}

interface Win extends PlayerEvent {
    type: 'WIN';
}
