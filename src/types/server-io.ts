import type {Command} from '../game/types/game-commands';
import type {PlayerView} from './game';

export type Request =
    SyncRequest
    | SetNameRequest
    | JoinRoomRequest
    | LeaveRoomRequest
    | DeleteRoomRequest
    | StartGameRequest
    | SendGameCommandRequest;

interface SyncRequest {
    type: 'SYNC';
}

interface SetNameRequest {
    type: 'SET_NAME';
    name: string;
}

interface JoinRoomRequest {
    type: 'JOIN_ROOM';
    roomId: string | null;
}

interface LeaveRoomRequest {
    type: 'LEAVE_ROOM';
}

interface DeleteRoomRequest {
    type: 'DELETE_ROOM';
}

interface StartGameRequest {
    type: 'START_GAME';
    history: boolean;
}

interface SendGameCommandRequest {
    type: 'SEND_GAME_COMMAND';
    command: Command;
}

export type Response = DataResponse | ErrorResponse;

interface DataResponse {
    type: 'DATA',
    name: string | null,
    room: RoomInfo | null,
    game: PlayerView | null,
}

interface ErrorResponse {
    type: 'ERROR'
    message: string,
}

export interface RoomInfo {
    id: string;
    host: boolean;
    members: string[];
}