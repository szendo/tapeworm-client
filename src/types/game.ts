import type {Action, Card, Color, Coord, Tile} from './base';
import type {GameEvent} from './events';

export interface PlayerView {
    thisPlayer: number;
    currentPlayer: number;
    winner: number | null;
    playerCardCounts: number[];
    selectedColor: Color | null;
    lastCoord: Coord | null;
    lastPlayedCard: Card | null;
    lastCutTiles: Tile[] | null;
    queuedAction: Action | null;
    queuedRingworms: 0 | 1 | 2 | 3;
    deckSize: number;
    discardSize: number;
    targetPlayer: number | null;
    targetHand: Card[] | null;
    tiles: Tile[];
    hand: Card[];
    openTiles: Coord[];
    history: GameEvent[];
}