import {useCallback, useEffect, useRef, useState} from 'react';
import './app.css';
import type {Command} from './game/types/game-commands';
import type {Request, Response, RoomInfo} from './types/server-io';
import type {PlayerView} from './types/game';
import Game from './game/game';
import LobbyPanel from './ui/lobby-panel';
import LoginPanel from './ui/login-panel';
import RoomPanel from './ui/room-panel';

interface AppState {
    connected: boolean;
    name: string | null;
    room: RoomInfo | null;
    game: PlayerView | null;
}

function App() {
    const [{connected, name, room, game}, setAppState] = useState<AppState>({
        connected: false,
        name: null,
        room: null,
        game: null,
    });

    const wsRef = useRef<WebSocket>();

    const sendRequest = useCallback((request: Request) => {
        const ws = wsRef.current!!;
        if (ws.readyState === WebSocket.OPEN) {
            ws.send(JSON.stringify(request));
        }
    }, []);

    const sendSync = useCallback(() => {
        sendRequest({type: 'SYNC'});
    }, [sendRequest]);

    const sendSetName = useCallback((name: string) => {
        sendRequest({type: 'SET_NAME', name: name});
    }, [sendRequest]);

    const sendJoinRoom = useCallback((room: string) => {
        sendRequest({
            type: 'JOIN_ROOM',
            roomId: room.length === 0 ? null : room
        });
    }, [sendRequest]);

    const startGameRequest = useCallback((history: boolean) => {
        sendRequest({type: 'START_GAME', history});
    }, [sendRequest]);

    const sendGameCommand = useCallback((command: Command) => {
        sendRequest({type: 'SEND_GAME_COMMAND', command});
    }, [sendRequest]);

    useEffect(() => {
        const ws = wsRef.current = new WebSocket(process.env.REACT_APP_SERVER_URL!);
        ws.addEventListener('message', ({data}) => {
            const response = JSON.parse(data) as Response
            switch (response.type) {
                case 'DATA':
                    setAppState(state => ({
                        ...state,
                        connected: true,
                        name: response.name,
                        room: response.room,
                        game: response.game,
                    }));
                    break;

                case 'ERROR':
                    console.log('error', response.message);
                    ws.send(JSON.stringify({type: 'SYNC'}));
                    break;
            }
        });

        return () => ws.close();
    }, []);

    return (
        <>
            {!connected
                ? 'Not connected...'
                : name === null
                    ? <LoginPanel onNameSet={sendSetName}/>
                    : room === null
                        ? <LobbyPanel name={name} onRoomJoin={sendJoinRoom}/>
                        : game == null
                            ? <RoomPanel name={name} room={room} onStartGame={startGameRequest}/>
                            : <Game players={room.members} game={game}
                                    sendCommand={sendGameCommand}
                                    restartGame={room.host ? startGameRequest : undefined}/>}
        </>
    );
}

export default App;
