import {useState} from 'react';
import './room-panel.css';
import type {RoomInfo} from '../types/server-io';

interface RoomPanelProps {
    name: string;
    room: RoomInfo;
    onStartGame: { (history: boolean): void; }
}

function RoomPanel({name, room, onStartGame}: RoomPanelProps) {
    const [history, setHistory] = useState(true);

    return (
        <div className='RoomPanel'>
            <form className='RoomPanel__form'
                  onSubmit={e => e.preventDefault()}>
                <div className='RoomPanel__info-line'>Name: {name}</div>
                <div className='RoomPanel__info-line'>
                    Room: <span className='RoomPanel__room-name'>{room.id}</span>
                </div>
                <div className='RoomPanel__info-line'>
                    Players:
                    <ul className='RoomPanel__player-list'>
                        {room.members.map((member, index) => <li key={index}>{member}</li>)}
                    </ul>
                </div>
                {room.host &&
                <div className='RoomPanel__info-line'>
                    <label className='RoomPanel__history-toggle'>
                        <input type='checkbox' checked={history}
                               onChange={(e) => setHistory(e.target.checked)}/> Enable history
                    </label>
                </div>}
                {room.host
                    ? <button className='RoomPanel__start-button'
                              disabled={room.members.length < 2 || room.members.length > 8}
                              onClick={() => onStartGame(history)}>Start game</button>
                    : <div className='RoomPanel__info-line'>Wait for the host to start the game</div>}
                <div className='LobbyPanel__info-text'>Rules: <a href={process.env.PUBLIC_URL+ '/rules.pdf'}>PDF</a></div>
            </form>

        </div>
    );
}

export default RoomPanel;
