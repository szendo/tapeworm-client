import {useState} from 'react';
import './login-panel.css';

interface LoginPanelProps {
    onNameSet: { (name: string): void;}
}

function LoginPanel({onNameSet}: LoginPanelProps) {
    const [name, setName] = useState('');

    return (
        <div className='LoginPanel'>
            <form className='LoginPanel__form' onSubmit={e => {
                e.preventDefault();
                if (name.length > 0) {
                    onNameSet(name);
                }
            }}>
                <div className='LoginPanel__info-text'>Choose a name</div>
                <label>
                    <input className='LoginPanel__name-input'
                           autoFocus required maxLength={30}
                           placeholder='Name'
                           onChange={e => setName(e.target.value)}/>
                </label>
                <button className='LoginPanel__submit-button'
                    type='submit' disabled={name.length === 0}>Go</button>
            </form>

        </div>
    );
}

export default LoginPanel;
