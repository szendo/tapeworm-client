import {useState} from 'react';
import './lobby-panel.css'

interface LobbyPanelProps {
    name: string;
    onRoomJoin: { (name: string): void; }
}

function LobbyPanel({name, onRoomJoin}: LobbyPanelProps) {
    const [roomId, setRoomId] = useState('');

    return (
        <div className='LobbyPanel'>
            <form className='LobbyPanel__form'
                onSubmit={e => {
                e.preventDefault();
                onRoomJoin(roomId);
            }}>
                <div className='LobbyPanel__info-text'>Create or join a room</div>
                <input className='LobbyPanel__name-input'
                    autoFocus maxLength={30}
                    placeholder='Room'
                    onChange={e => setRoomId(e.target.value)}/>
                <button className='LobbyPanel__submit-button'
                    type='submit'>Create / Join</button>
                <div className='LobbyPanel__info-text'>Rules: <a href={process.env.PUBLIC_URL+ '/rules.pdf'}>PDF</a></div>
            </form>

        </div>
    );
}

export default LobbyPanel;
