import './tile.css';
import type {Coord} from '../types/base';

interface MarkCutTileProps {
    coord: Coord;
}

function MarkCutTile({coord}: MarkCutTileProps) {
    return (
        <div className='Tile mark-cut click-through' style={{left: coord[0] * 64 + 'px', top: coord[1] * 64 + 'px'}}/>
    );
}

export default MarkCutTile;
