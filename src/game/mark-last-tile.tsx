import './tile.css';
import type {Coord} from '../types/base';

interface MarkLastTileProps {
    coord: Coord;
}

function MarkLastTile({coord}: MarkLastTileProps) {
    return (
        <div className='Tile mark-last click-through' style={{left: coord[0] * 64 + 'px', top: coord[1] * 64 + 'px'}}/>
    );
}

export default MarkLastTile;
