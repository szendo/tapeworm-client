import type {MouseEvent, WheelEvent} from 'react';
import React, {useCallback, useState} from 'react';
import './board-container.css';

const BoardContainer: React.FC = ({children}) => {
    const [position, setPosition] = useState({
        moving: false,
        x: 0,
        y: 0,
        scale: 1
    });

    const [boardRect, setBoardRect] = useState({
        top: 0, left: 0, width: 0, height: 0
    });

    const rectRef = useCallback((container: HTMLDivElement) => {
        if (container == null) {
            return
        }
        let {left, top, width, height} = container.getBoundingClientRect();
        setBoardRect({left, top, width, height});
    }, [])

    const startMoving = useCallback(() => {
        setPosition(pos => ({...pos, moving: true}));
    }, []);

    const stopMoving = useCallback(() => {
        setPosition(pos => ({...pos, moving: false}));
    }, []);

    const updateCoords = useCallback((e: MouseEvent<HTMLDivElement>) => {
        setPosition(pos => pos.moving ? {...pos, x: pos.x + e.movementX, y: pos.y + e.movementY} : pos);
    }, []);

    const updateZoom = useCallback((e: WheelEvent<HTMLDivElement>) => {
        setPosition(pos => {
            let oldScale = pos.scale;
            let newScale = Math.round(100 * Math.min(Math.max(oldScale - e.deltaY * 0.01, 0.5), 3)) / 100;
            if (oldScale === newScale) return pos;

            let cursorX = e.clientX - boardRect.left;
            let cursorY = e.clientY - boardRect.top;
            return ({
                ...pos,
                x: Math.round(100 * (cursorX - (cursorX - pos.x) * newScale / oldScale)) / 100,
                y: Math.round(100 * (cursorY - (cursorY - pos.y) * newScale / oldScale)) / 100,
                scale: newScale
            });
        });
    }, [boardRect]);

/*
    const resetPositionAndZoom = useCallback(() => {
        setPosition(pos => ({...pos, x: 0, y: 0, scale: 1}));
    }, []);
*/

    return (
        <div ref={rectRef}
             className={'BoardContainer' + (position.moving ? ' grabbing' : '')}
             onMouseDown={startMoving}
             onMouseUp={stopMoving}
             onMouseMove={updateCoords}
             onWheel={updateZoom}
        >
            <div style={{
                position: 'relative',
                transformOrigin: '0 0',
                transform: `translate(${position.x}px, ${position.y}px) scale(${position.scale})`
            }}
            >
                {children}
            </div>

            {/*<button style={{position: 'absolute', top: 80, left: 16}} onClick={resetPositionAndZoom}>reset zoom</button>*/}
        </div>
    );
};

export default BoardContainer;