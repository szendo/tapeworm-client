import './status-bar.css';
import type {PlayerView} from '../types/game';

interface StatusBarProps {
    players: string[];
    game: PlayerView;
}

function StatusBar({players, game}: StatusBarProps) {
    function getStatusText() {
        if (game.winner !== null) {
            return (game.winner === game.thisPlayer) ? 'You won' : `${players[game.winner]} won`;
        }

        if (game.thisPlayer !== game.currentPlayer) {
            if (game.lastPlayedCard != null) {
                return (<>
                    {players[game.currentPlayer]} played
                    {' '}
                    <img className='StatusBar__card-icon'
                         src={`${process.env.PUBLIC_URL}/images/cards/${game.lastPlayedCard.imageId}.jpg`} alt='card'/>

                </>)
            } else
            return `It's ${players[game.currentPlayer]}'s turn`
        }

        const action = game.queuedAction;
        switch (action?.type) {
            case 'DIG':
                return `Dig: discard ${action.strength} card(s)`;
            case 'HATCH':
                return `Hatch: force another player to draw ${action.strength} card(s)`;
            case 'PEEK':
                return 'Peek: place a card on the top of the deck';
            case 'SWAP':
                return 'Swap: ' + ((game.targetPlayer === null)
                    ? 'select a player to swap cards with'
                    : `swap up to ${action.strength} card(s)`);
        }

        if (game.queuedRingworms > 0) {
            return `Ringworm: discard ${game.queuedRingworms * 2} cards`;
        }

        return 'Play a card or end the turn';
    }

    return (
        <div className='StatusBar'>
            <span className={'StatusBar__text ' + game.selectedColor + (game.selectedColor !== null ? ' StatusBar__text--selected-color' : '')}>
                {getStatusText()}
            </span>
        </div>
    );
}

export default StatusBar;