import './player-list.css';
import type {PlayerView} from '../types/game';

interface PlayerListProps {
    players: string[],
    game: PlayerView
}

function PlayerList({players, game}: PlayerListProps) {
    return (
        <div className='PlayerList'>
            <div>Deck: {game.deckSize}</div>
            <div>Discard: {game.discardSize}</div>
            {players.map((player, index: number) =>
                <div key={index} className={'PlayerList__player' + (index === game.currentPlayer ? ' current-player' : '')}>
                    <div className='PlayerList__player-avatar'/>
                    <div className='PlayerList__player-name'>
                        {player}
                        {index === game.thisPlayer && ' (you)'}
                    </div>
                    <div className='PlayerList__card-count'>
                        <img className='PlayerList__card-icon'
                             src={`${process.env.PUBLIC_URL}/images/cards/back.jpg`} alt='card'/>
                        ×{game.playerCardCounts[index]}
                    </div>
                </div>
            )}
        </div>
    );
}

export default PlayerList;
