import './action-bar.css';
import type {Coord, TileCard} from '../types/base';
import {Direction} from '../types/base';
import type {PlayerView} from '../types/game';
import type {ClientState} from './types/client-state';
import type {Command} from './types/game-commands';

interface ActionBarProps {
    players: string[],
    clientState: ClientState,
    game: PlayerView,
    sendCommand: { (command: Command): void; }
    restartGame?: { (history: boolean): void; }
}

function ActionBar({players, clientState, game, sendCommand, restartGame}: ActionBarProps) {

    function sameCoords(c1: Coord | null, c2: Coord | null): boolean {
        return c1 === null ? c2 === null : c2 !== null && c1[0] === c2[0] && c1[1] === c2[1];
    }

    function moveInDir(c: Coord, d: Direction): Coord {
        switch (d) {
            case 0:
                return [c[0], c[1] - 1];
            case 1:
                return [c[0] + 1, c[1]];
            case 2:
                return [c[0], c[1] + 1];
            case 3:
                return [c[0] - 1, c[1]];
        }
    }

    function isValidTileCardToPlay(parentDir: Direction) {
        const card = clientState.cardToPlay.card;
        if (card === null || card.type !== 'TILE') {
            return false;
        }
        const coord: Coord | null = clientState.cardToPlay.coord;
        if (coord === null) {
            return false;
        }

        const parentTile = game.tiles.find((tile) => sameCoords(tile.coord, moveInDir(coord, parentDir)));
        if (parentTile === undefined) return false;

        const ownEdge = card.edges[(parentDir + 4 - clientState.cardToPlay.rotation) % 4];
        if (ownEdge === null) {
            return false;
        }

        if (game.lastCoord !== null && !sameCoords(game.lastCoord, parentTile.coord)) {
            return false
        }

        if (game.selectedColor !== null) {
            if (ownEdge !== game.selectedColor) {
                return false;
            }
        }

        return ([0, 1, 2, 3] as Direction[]).every(dir => {
            const tile = game.tiles.find((tile) => sameCoords(tile.coord, moveInDir(coord, dir)));
            if (tile === undefined) return true;
            const cardEdge = card.edges[(dir + 4 - clientState.cardToPlay.rotation) % 4];
            const neighborEdge = tile.card.edges[(dir + 6 - tile.rotation) % 4];
            return cardEdge === neighborEdge;
        });
    }

    function isValidCutCardToPlay() {
        const card = clientState.cardToPlay.card;
        if (card === null || card.type !== 'CUT') {
            return false
        }
        const coord: Coord | null = clientState.cardToPlay.coord;
        if (coord === null) {
            return false
        }

        const cutTile = game.tiles.find((tile) => sameCoords(tile.coord, coord));
        if (cutTile === undefined) return false;

        if (cutTile.cutColor === null) {
            return false;
        }

        if (game.selectedColor !== null) {
            if (cutTile.cutColor !== game.selectedColor) {
                return false;
            }
        }

        return card.color === null || card.color === cutTile.cutColor;
    }

    return (
        <div className='ActionBar'>
            {game.thisPlayer === game.currentPlayer && <>
                {clientState.playingTileCard && clientState.cardToPlay.coord !== null && ([0, 1, 2, 3] as Direction[])
                    .filter(parentDir => isValidTileCardToPlay(parentDir))
                    .map(parentDir => (
                        <button key={parentDir}
                                onClick={() => {
                                    if (clientState.cardToPlay.coord !== null) {
                                        sendCommand({
                                            type: 'PLAY_CARD',
                                            cardIndex: clientState.ownSelected[0],
                                            coord: clientState.cardToPlay.coord,
                                            rotation: clientState.cardToPlay.rotation,
                                            direction: parentDir,
                                        });
                                    }
                                }}>Play Card ({(clientState.cardToPlay.card as TileCard).edges[(parentDir + 4 - clientState.cardToPlay.rotation) % 4]})</button>))}

                {clientState.playingCutCard && isValidCutCardToPlay() &&
                <button onClick={() => {
                    if (clientState.cardToPlay.coord !== null) {
                        sendCommand({
                            type: 'PLAY_CARD',
                            cardIndex: clientState.ownSelected[0],
                            coord: clientState.cardToPlay.coord,
                            rotation: 0,
                            direction: 0,
                        });
                    }
                }}>Cut ({game.tiles.find(tile => sameCoords(tile.coord, clientState.cardToPlay.coord))?.cutColor})</button>}

                {game.queuedAction?.type === 'DIG' && game.queuedAction.strength === clientState.ownSelected.length &&
                <button onClick={() => {
                    sendCommand({type: 'DIG_DISCARD', cardIndices: clientState.ownSelected})

                }}>Discard</button>}

                {game.queuedAction === null && game.queuedRingworms > 0 &&
                game.queuedRingworms * 2 === clientState.ownSelected.length &&
                <button onClick={() => {
                    sendCommand({type: 'RINGWORM_DISCARD', cardIndices: clientState.ownSelected})

                }}>Discard</button>}

                {game.queuedAction?.type === 'PEEK' && 1 === clientState.ownSelected.length &&
                <button onClick={() => {
                    sendCommand({type: 'PEEK_UNDRAW', cardIndex: clientState.ownSelected[0]})
                }}>Place on deck</button>}

                {game.queuedAction?.type === 'HATCH' && players.map((_, index) => index)
                    .filter(index => index !== game.thisPlayer)
                    .map(index => <button key={index} onClick={() => {
                        sendCommand({type: 'HATCH_TARGET', playerIndex: index})
                    }}>{players[index]}</button>)}

                {game.queuedAction?.type === 'SWAP' && game.targetPlayer === null && players.map((_, index) => index)
                    .filter(index => index !== game.thisPlayer)
                    .map(index => <button key={index} onClick={() => {
                        sendCommand({type: 'SWAP_TARGET', playerIndex: index})
                    }}>{players[index]}</button>)}

                {game.queuedAction?.type === 'SWAP' && game.targetPlayer !== null &&
                game.queuedAction.strength >= clientState.ownSelected.length &&
                clientState.ownSelected.length === clientState.targetSelected.length &&
                <button onClick={() => {
                    sendCommand({
                        type: 'SWAP_CARDS',
                        ownCardIndices: clientState.ownSelected,
                        targetCardIndices: clientState.targetSelected
                    })
                }}>Swap {clientState.ownSelected.length} card(s)</button>}

                {game.winner === null && game.queuedAction === null && game.queuedRingworms === 0 &&
                0 === clientState.ownSelected.length &&
                <button className='danger' onClick={() => sendCommand({type: 'END_TURN'})}>End turn</button>}
            </>}

            {game.winner !== null && restartGame !== undefined &&
            <button onClick={() => restartGame(game.history.length > 0)}>New game</button>}
        </div>
    );
}

export default ActionBar;