import './tile.css';
import type {Card, Coord, Rotation} from '../types/base';

interface CutGhostTileProps {
    card: Card;
    coord: Coord;
    rotation: Rotation;
}

function CutGhostTile({card, coord, rotation}: CutGhostTileProps) {
    return (
        <div className='Tile click-through'
             style={{
                 left: coord[0] * 64 + 'px',
                 top: coord[1] * 64 + 'px',
             }}>
            <img className={'Tile__card ghost rot-' + rotation}
                 src={`${process.env.PUBLIC_URL}/images/cards/${card.imageId}.jpg`} alt='card'/>
        </div>
    );
}

export default CutGhostTile;
