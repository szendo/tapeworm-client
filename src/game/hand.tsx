import './hand.css';
import type {Card} from '../types/base';

interface HandProps {
    hand: Card[];
    selected: number[];
    toggleSelection: (index: number) => void
}

function Hand({hand, selected, toggleSelection}: HandProps) {
    return (
        <div className='Hand'>
            {hand.map((card: Card, index: number) =>
                <div key={index}
                     className={'Hand__card' + (selected.indexOf(index) !== -1 ? ' selected' : '') + ' active'}
                     onClick={() => toggleSelection(index)}>
                    <img key={card.imageId} className={'Hand__card-image'}
                         src={process.env.PUBLIC_URL + '/images/cards/' + card.imageId + '.jpg'} alt='card'/>
                </div>
            )}
        </div>
    );
}

export default Hand;
