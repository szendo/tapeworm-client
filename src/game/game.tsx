import {useEffect} from 'react';
import './game.css'
import type {PlayerView} from '../types/game';
import type {Command} from './types/game-commands';
import {ActionType} from './types/client-state';
import ActionBar from './action-bar';
import Board from './board';
import BoardContainer from './board-container';
import GameHistory from './game-history';
import Hand from './hand';
import PlayerList from './player-list';
import StatusBar from './status-bar';
import useClientState from './hooks/use-client-state';

export interface GameProps {
    players: string[];
    game: PlayerView;
    sendCommand: { (command: Command): void; }
    restartGame?: { (history: boolean): void; }
}

function Game({players, game, sendCommand, restartGame}: GameProps) {
    const [clientState, dispatch] = useClientState(game);

    // reset the client state when the game changes
    useEffect(() => {
        dispatch({
            type: ActionType.Sync,
            game: game
        })
    }, [game, dispatch]);

    return (
        <div className='Game'>
            <div className='Game__play-area'>
                <div className='Game__board-area'>
                    <div className='Game__status-bar'>
                        <StatusBar players={players} game={game}/>
                    </div>
                    <div className='Game__action-bar'>
                        <ActionBar players={players} clientState={clientState} game={game}
                                   sendCommand={sendCommand} restartGame={restartGame}/>
                    </div>
                    <div className='Game__board'>
                        <BoardContainer>
                            <Board clientState={clientState} game={game} dispatch={dispatch}/>
                        </BoardContainer>
                    </div>
                </div>
                <div className='Game__right-panel'>
                    <div className='Game__player-list'>
                        <PlayerList players={players} game={game}/>
                    </div>
                    <div className='Game__history'>
                        <GameHistory players={players} history={game.history}/>
                    </div>

                </div>
            </div>

            <div className='Game__own-hand'>
                <Hand hand={game.hand}
                      selected={clientState.ownSelected}
                      toggleSelection={(index: number) => {
                          dispatch({
                              type: ActionType.ToggleOwnCardSelection,
                              game: game,
                              cardIndex: index,
                          });
                      }}/>
            </div>


            {game.targetHand !== null &&
            <div className='Game__target-hand'>
                <Hand hand={game.targetHand}
                      selected={clientState.targetSelected}
                      toggleSelection={(index: number) => {
                          dispatch({
                              type: ActionType.ToggleTargetCardSelection,
                              cardIndex: index,
                          });
                      }}/>
            </div>}

        </div>
    );
}

export default Game;
