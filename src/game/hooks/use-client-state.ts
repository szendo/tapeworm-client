import {useReducer} from 'react';
import type {Coord, CutCard, Rotation} from '../../types/base';
import type {PlayerView} from '../../types/game';
import type {
    Action,
    ClientState,
    SelectCardCoordAction,
    ToggleOwnCardSelectionAction,
    ToggleTargetCardSelectionAction
} from '../types/client-state';
import {ActionType} from '../types/client-state';

function coordKey(c: Coord): string {
    return c[0] + ';' + c[1];
}

function sameCoords(c1: Coord | null, c2: Coord | null): boolean {
    return c1 == null ? c2 == null : c2 != null && c1[0] === c2[0] && c1[1] === c2[1];
}

function rotateCw(rot: Rotation): Rotation {
    return (rot + 1) % 4 as Rotation;
}

function reducer(state: ClientState, action: Action): ClientState {
    switch (action.type) {
        case ActionType.Sync:
            return sync(action.game);
        case ActionType.ToggleOwnCardSelection:
            return toggleOwnCardSelection(state, action);
        case ActionType.ToggleTargetCardSelection:
            return toggleTargetCardSelection(state, action);
        case ActionType.SelectCardCoord:
            return selectCardCoord(state, action);
    }
}

function sync(game: PlayerView): ClientState {
    const ownTurn = game.thisPlayer === game.currentPlayer;
    const selectionLimit = game.winner == null && ownTurn
        ? game.queuedAction !== null
            ? game.queuedAction.type === 'PEEK'
                ? 1 : game.queuedAction.type === 'HATCH' ? 0 : game.queuedAction.strength
            : game.queuedRingworms > 0 ? 2 * game.queuedRingworms : 1
        : 0;
    const playingCard = ownTurn && game.queuedAction === null && game.queuedRingworms === 0;

    return {
        ownTurn: ownTurn,
        selectionLimit: selectionLimit,
        ownSelected: [],
        targetSelected: [],
        playingCard: playingCard,
        playingTileCard: false,
        playingCutCard: false,
        cardToPlay: {card: null, coord: null, rotation: 0},
        coordsToCut: [],
    };
}

function toggleOwnCardSelection(state: ClientState, {game, cardIndex}: ToggleOwnCardSelectionAction): ClientState {
    if (state.selectionLimit === 0) {
        return state;
    }

    let oldOwnSelected = state.ownSelected;
    let indexOfIndex = oldOwnSelected.indexOf(cardIndex);
    let newOwnSelected: number[];
    if (indexOfIndex !== -1) {
        newOwnSelected = [...oldOwnSelected.slice(0, indexOfIndex), ...oldOwnSelected.slice(indexOfIndex + 1, oldOwnSelected.length)]
    } else {
        newOwnSelected = [...oldOwnSelected, cardIndex];
        if (newOwnSelected.length > state.selectionLimit) {
            newOwnSelected.splice(0, newOwnSelected.length - state.selectionLimit);
        }
    }

    let card = indexOfIndex === -1 ? game.hand[cardIndex] : null;
    return {
        ...state,
        ownSelected: newOwnSelected,
        playingTileCard: state.playingCard && card?.type === 'TILE',
        playingCutCard: state.playingCard && card?.type === 'CUT',
        cardToPlay: {card: card, coord: null, rotation: 0},
        coordsToCut: [],
    }
}

function toggleTargetCardSelection(state: ClientState, {cardIndex}: ToggleTargetCardSelectionAction): ClientState {
    if (state.selectionLimit === 0) {
        return state;
    }

    let oldTgtSelected = state.targetSelected;
    let indexOfIndex = oldTgtSelected.indexOf(cardIndex);
    let newTgtSelected: number[];
    if (indexOfIndex !== -1) {
        newTgtSelected = [...oldTgtSelected.slice(0, indexOfIndex), ...oldTgtSelected.slice(indexOfIndex + 1, oldTgtSelected.length)]
    } else {
        newTgtSelected = [...oldTgtSelected, cardIndex];
        if (newTgtSelected.length > state.selectionLimit) {
            newTgtSelected.splice(0, newTgtSelected.length - state.selectionLimit);
        }
    }

    return {
        ...state,
        targetSelected: newTgtSelected,
    }
}

function selectCardCoord(state: ClientState, {game, coord}: SelectCardCoordAction): ClientState {
    if (!state.playingCard || state.ownSelected.length < 1) {
        return state
    }

    const tileOnCoord = game.tiles.find((tile) => sameCoords(tile.coord, coord));

    switch (state.cardToPlay.card?.type) {
        case 'TILE': {
            if (tileOnCoord !== undefined) return state;

            return {
                ...state,
                cardToPlay: {
                    ...state.cardToPlay,
                    coord: coord,
                    rotation: sameCoords(state.cardToPlay.coord, coord) ? rotateCw(state.cardToPlay.rotation) : 0,
                },
                coordsToCut: [],
            };
        }

        case 'CUT': {
            const cardColor = (state.cardToPlay.card as CutCard).color;
            if (tileOnCoord === undefined
                || tileOnCoord.cutColor === null
                || (game.selectedColor !== null && game.selectedColor !== tileOnCoord.cutColor)
                || (cardColor !== null && cardColor !== tileOnCoord.cutColor)) {
                return state;
            }

            const coordsToCut: Coord[] = [];
            if (state.cardToPlay.coord !== coord) {
                const coordsQueued = [coord];
                const coordKeysHandled: string[] = [];

                while (coordsQueued.length > 0) {
                    const spotCoord = coordsQueued.shift() as Coord;
                    if (coordKeysHandled.indexOf(coordKey(spotCoord)) !== -1) {
                        continue;
                    }

                    coordsToCut.push(spotCoord);
                    coordKeysHandled.push(coordKey(spotCoord));
                    coordsQueued.push(...(game.tiles
                        .filter(tile => tile.parents.some(parentCoord => sameCoords(parentCoord, spotCoord)))
                        .map(tile => tile.coord)))
                }
            }

            return {
                ...state,
                cardToPlay: {
                    ...state.cardToPlay,
                    coord: state.cardToPlay.coord === coord ? null : coord,
                    rotation: 0,
                },
                coordsToCut: coordsToCut,
            };
        }

        default:
            return state;
    }
}

export default function useClientState(game: PlayerView) {
    return useReducer(reducer, game, sync);
};
