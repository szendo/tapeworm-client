import {Coord, Direction, Rotation} from '../../types/base';

export type Command =
    SyncCommand
    | PlayCardCommand
    | DigDiscardCommand
    | PeekUndrawCommand
    | HatchTargetCommand
    | SwapTargetCommand
    | SwapCardsCommand
    | RingwormDiscardCommand
    | EndTurnCommand;

export interface SyncCommand {
    type: 'SYNC';
}

export interface PlayCardCommand {
    type: 'PLAY_CARD';
    cardIndex: number;
    coord: Coord;
    rotation: Rotation;
    direction: Direction;
}

export interface DigDiscardCommand {
    type: 'DIG_DISCARD';
    cardIndices: number[];
}

export interface PeekUndrawCommand {
    type: 'PEEK_UNDRAW';
    cardIndex: number;
}

export interface HatchTargetCommand {
    type: 'HATCH_TARGET';
    playerIndex: number;
}

export interface SwapTargetCommand {
    type: 'SWAP_TARGET';
    playerIndex: number;
}

export interface SwapCardsCommand {
    type: 'SWAP_CARDS';
    ownCardIndices: number[];
    targetCardIndices: number[];
}

export interface RingwormDiscardCommand {
    type: 'RINGWORM_DISCARD';
    cardIndices: number[];
}

export interface EndTurnCommand {
    type: 'END_TURN';
}
