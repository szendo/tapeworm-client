import type {Card, Coord, Rotation} from '../../types/base';
import type {PlayerView} from '../../types/game';

export interface ClientState {
    ownTurn: boolean;
    selectionLimit: number;
    ownSelected: number[];
    targetSelected: number[];
    playingCard: boolean;
    playingTileCard: boolean;
    playingCutCard: boolean;
    cardToPlay: { card: Card | null, coord: Coord | null, rotation: Rotation };
    coordsToCut: Coord[];
}

export type Action =
    SyncAction
    | ToggleOwnCardSelectionAction
    | ToggleTargetCardSelectionAction
    | SelectCardCoordAction;

export enum ActionType {Sync, ToggleOwnCardSelection, ToggleTargetCardSelection, SelectCardCoord}

export interface SyncAction {
    type: ActionType.Sync;
    game: PlayerView;
}

export interface ToggleOwnCardSelectionAction {
    type: ActionType.ToggleOwnCardSelection;
    game: PlayerView;
    cardIndex: number;
}

export interface ToggleTargetCardSelectionAction {
    type: ActionType.ToggleTargetCardSelection;
    cardIndex: number;
}

export interface SelectCardCoordAction {
    type: ActionType.SelectCardCoord;
    game: PlayerView;
    coord: Coord;
}
