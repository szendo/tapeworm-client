import type {ReactNode} from 'react';
import './game-history.css';
import type {Action} from '../types/base';
import type {GameEvent} from '../types/events';

interface GameHistoryProps {
    players: string[];
    history: GameEvent[];
}

function drawCards(imageIds: string[]) {
    return imageIds.map(drawCard);
}

function drawCard(imageId: string, key?: number) {
    return <img className='History__card-icon' key={key}
                src={`${process.env.PUBLIC_URL}/images/cards/${imageId === '' ? 'back' : imageId}.jpg`} alt='card'/>;
}

function describeAction(action: Action) {
    switch (action.type) {
        case 'DIG':
        case 'HATCH':
        case 'SWAP':
            return action.strength > 1 ? `${action.type}×${action.strength}` : action.type;
        case 'PEEK':
            return action.type;
    }

}

function mapReverse(history: GameEvent[], fn: { (e: GameEvent, index: number): ReactNode}): ReactNode[] {
    const historyLength = history.length
    return history.slice().reverse().map((e, i) => fn(e, historyLength - i));
}

function GameHistory({players, history}: GameHistoryProps) {
    return (
        <div className='History'>
            {mapReverse(history, (event, index) => {
                switch (event.type) {
                    case 'RESHUFFLE_DECK':
                        return (<div key={index} className='History__game-event'>Deck empty, reshuffling discard pile</div>);

                    case 'START_TURN':
                        return (<div key={index} className='History__game-event'>{players[event.playerIndex]} starts turn</div>);

                    case 'DRAW_CARDS':
                        return (<div key={index} className='History__game-event'>{players[event.playerIndex]} draws {drawCards(event.cards)}</div>);

                    case 'PLAY_TILE_CARD':
                        return (<div key={index} className='History__game-event'>{players[event.playerIndex]} plays {drawCard(event.card)}</div>);

                    case 'PLAY_CUT_CARD':
                        return (<div key={index} className='History__game-event'>{players[event.playerIndex]} plays {drawCard(event.card)}, cuts {drawCards(event.cutCards)}</div>);

                    case 'SELECT_COLOR':
                        return (<div key={index} className='History__game-event'>{players[event.playerIndex]} selects color: {event.color}</div>);

                    case 'TRIGGER_ACTION':
                        return (<div key={index} className='History__game-event'>{players[event.playerIndex]} triggers action: {describeAction(event.action)}</div>);

                    case 'DISCARD_CARDS':
                        return (<div key={index} className='History__game-event'>{players[event.playerIndex]} discards {drawCards(event.cards)}</div>);

                    case 'FORCE_DRAW':
                        return (<div key={index} className='History__game-event'>{players[event.playerIndex]} forces {players[event.targetPlayerIndex]} to draw {event.count}</div>);

                    case 'UNDRAW_CARD':
                        return (<div key={index} className='History__game-event'>{players[event.playerIndex]} places {drawCard(event.card)} on top of the deck</div>);

                    case 'SELECT_SWAP_TARGET':
                        return (<div key={index} className='History__game-event'>{players[event.playerIndex]} decides to swap with {players[event.targetPlayerIndex]}</div>);

                    case 'SWAP_CARDS':
                        if (event.cardsGiven.length > 0) {
                            return (<div key={index} className='History__game-event'>{players[event.playerIndex]} swaps with {players[event.targetPlayerIndex]}:
                                {' '}
                                {drawCards(event.cardsGiven)}↔{drawCards(event.cardsTaken)}
                            </div>);
                        } else {
                            return (<div key={index} className='History__game-event'>{players[event.playerIndex]} does not swap any cards with {players[event.targetPlayerIndex]}</div>);
                        }

                    case 'COMPLETE_RINGWORM':
                        if (event.ringwormCount > 1) {
                            return (<div key={index} className='History__game-event'>{players[event.playerIndex]} completes {event.ringwormCount} ringworms</div>);
                        } else {
                            return (<div key={index} className='History__game-event'>{players[event.playerIndex]} completes a ringworm</div>);
                        }

                    case 'END_TURN':
                            return (<div key={index} className='History__game-event'>{players[event.playerIndex]} ends turn</div>);

                    case 'WIN':
                        return (<div key={index} className='History__game-event'>{players[event.playerIndex]} wins</div>);
                        
                    default:
                        return (<div key={index} className='History__game-event'>Unknown event</div>)
                }
            })}
            {history.length === 0 && <div>History not enabled</div> }
        </div>
    );
}

export default GameHistory;
