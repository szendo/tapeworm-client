import type {MouseEventHandler} from 'react';
import './tile.css';
import type {Tile as TileType} from '../types/base';

interface TileProps {
    tile: TileType;
    active: boolean;
    disabled: boolean;
    onClick: MouseEventHandler;
}

function Tile({tile, active, disabled, onClick}: TileProps) {
    return (
        <div className={'Tile' + (active ? ' active' : '') + (disabled ? ' disabled' : '')}
             style={{left: tile.coord[0] * 64 + 'px', top: tile.coord[1] * 64 + 'px'}}
             onClick={onClick}
             onMouseDownCapture={e => e.preventDefault()}
             onMouseMoveCapture={e => e.preventDefault()}
             onMouseUpCapture={e => e.preventDefault()}>
            <img className={'Tile__card rot-' + tile.rotation}
                 src={`${process.env.PUBLIC_URL}/images/cards/${tile.card.imageId}.jpg`} alt='card'/>
        </div>
    );
}

export default Tile;
