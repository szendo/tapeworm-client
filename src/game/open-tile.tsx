import type {MouseEventHandler} from 'react';
import './tile.css';
import type {Coord} from '../types/base';

interface OpenTileProps {
    coord: Coord;
    active: boolean;
    onClick: MouseEventHandler;
}

function OpenTile({coord, active, onClick}: OpenTileProps) {
    return (
        <div className={'Tile open' + (active ? ' active' : '')}
             style={{
                 left: coord[0] * 64 + 'px',
                 top: coord[1] * 64 + 'px',
             }} onClick={onClick}/>
    );
}

export default OpenTile;
