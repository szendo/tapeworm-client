import type {Coord, CutCard, TileCard} from '../types/base';
import type {PlayerView} from '../types/game';
import type {Action, ClientState} from './types/client-state';
import {ActionType} from './types/client-state';
import CutGhostTile from './cut-ghost-tile';
import MarkCutTile from './mark-cut-tile';
import MarkLastTile from './mark-last-tile';
import OpenTile from './open-tile';
import MarkPlaceTile from './mark-place-tile';
import Tile from './tile';

interface BoardProps {
    clientState: ClientState;
    game: PlayerView;
    dispatch: { (action: Action): void; }
}

function Board({clientState, game, dispatch}: BoardProps) {

    function sameCoords(c1: Coord | null, c2: Coord | null): boolean {
        return c1 === null ? c2 === null : c2 !== null && c1[0] === c2[0] && c1[1] === c2[1];
    }

    function coordKey(c: Coord): string {
        return c[0] + ';' + c[1];
    }

    return (
        <>
            {game.tiles.map((tile, index: number) =>
                <Tile key={coordKey(tile.coord)} tile={tile}
                      active={clientState.playingCutCard && tile.cutColor !== null}
                      disabled={clientState.playingCutCard && (tile.cutColor === null ||
                          (game.selectedColor !== null && tile.cutColor !== game.selectedColor) ||
                          ((clientState.cardToPlay.card as CutCard).color !== null && (clientState.cardToPlay.card as CutCard).color !== tile.cutColor))}
                      onClick={() => {
                          dispatch({
                              type: ActionType.SelectCardCoord,
                              game: game,
                              coord: tile.coord
                          });
                      }}
                />
            )}

            {game.lastCutTiles !== null && game.lastCutTiles.map((tile) =>
                <CutGhostTile key={coordKey(tile.coord)} card={tile.card} coord={tile.coord}
                              rotation={tile.rotation}/>)}

            {(game.lastCoord == null || true) && game.openTiles.map((coord) =>
                <OpenTile key={coordKey(coord)} coord={coord}
                          active={clientState.playingTileCard}
                          onClick={() => {
                              dispatch({
                                  type: ActionType.SelectCardCoord,
                                  game: game,
                                  coord: coord,
                              });
                          }}/>
            )}

            {game.lastCoord !== null && // !clientState.playingCutCard &&
            game.tiles.find(tile => sameCoords(tile.coord, game.lastCoord)) !== undefined &&
            <MarkLastTile coord={game.lastCoord}/>}

            {clientState.playingTileCard && clientState.cardToPlay.coord !== null &&
            <MarkPlaceTile card={clientState.cardToPlay.card as TileCard}
                           coord={clientState.cardToPlay.coord}
                           rotation={clientState.cardToPlay.rotation}
                           onClick={() => {
                               dispatch({
                                   type: ActionType.SelectCardCoord,
                                   game: game,
                                   coord: clientState.cardToPlay.coord as Coord,
                               });
                           }}/>}

            {clientState.coordsToCut.map(coord =>
                <MarkCutTile key={coordKey(coord)} coord={coord}/>)}
        </>
    );
}

export default Board;