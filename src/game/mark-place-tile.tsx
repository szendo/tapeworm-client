import type {MouseEventHandler} from 'react';
import './tile.css';
import type {Card, Coord, Rotation} from '../types/base';

interface MarkPlaceTileProps {
    card: Card;
    coord: Coord;
    rotation: Rotation;
    onClick: MouseEventHandler;
}

function MarkPlaceTile({card, coord, rotation, onClick}: MarkPlaceTileProps) {
    return (
        <div className='Tile active mark-place'
             style={{
                 left: coord[0] * 64 + 'px',
                 top: coord[1] * 64 + 'px',
             }} onClick={onClick}>
            <img className={'Tile__card rot-' + rotation}
                 src={`${process.env.PUBLIC_URL}/images/cards/${card.imageId}.jpg`} alt='card'/>
        </div>
    );
}

export default MarkPlaceTile;
