import {useCallback, useRef} from 'react';

export default function useWebSocket(url: string, onMessage: (data: any, sendMessage: (data: any) => boolean) => any) {
    const wsRef = useRef<WebSocket>();
    const sendMessage = useCallback((data: any): boolean => {
        let ws = wsRef.current!;
        switch (ws.readyState) {
            case WebSocket.OPEN:
                ws.send(JSON.stringify(data));
                return true

            case WebSocket.CONNECTING:
            case WebSocket.CLOSING:
            case WebSocket.CLOSED:
                return false
        }
        return false;
    }, []);

    if (wsRef.current === null) {
        wsRef.current = new WebSocket(url);
        wsRef.current?.addEventListener('message', (event) => {
            onMessage(JSON.parse(event.data), sendMessage)
        });
    }

    return [wsRef.current!.readyState, sendMessage];
};
