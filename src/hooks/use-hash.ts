import type {Dispatch, SetStateAction} from 'react';
import {useCallback, useEffect, useRef, useState} from 'react';

export function useHash(): [string, Dispatch<SetStateAction<string>>] {
    let initialHash = window.location.hash;
    const [hash, setHash] = useState(initialHash === '' ? '' : decodeURIComponent(initialHash.substring(1)));
    const cleanAndSetHash = useCallback((hashOrFn: SetStateAction<string>) => {
        setHash(typeof hashOrFn === 'function'
            ? ((hash) => hashOrFn(hash).replace(/^#+/, ''))
            : hashOrFn.replace(/^#+/, ''));
    }, []);

    useEffect(() => {
        const handleHashChange = (e: HashChangeEvent) => {
            let hashPos = e.newURL.indexOf('#');
            setHash((hashPos === -1) ? '' : decodeURIComponent(e.newURL.substring(hashPos + 1)));
        };

        window.addEventListener('hashchange', handleHashChange);
        return () => {
            window.removeEventListener('hashchange', handleHashChange);
        }
    }, []);

    const initial = useRef(true);
    useEffect(() => {
        if (initial.current) {
            initial.current = false;
        } else {
            window.location.hash = hash;
        }
    }, [hash]);

    return [hash, cleanAndSetHash];
}
